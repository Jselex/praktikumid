package praktikum5;

public class Paarisarvud {

	public static void main(String[] args) {

		// i++ on sama mis i = i + 1
		// i += 2 on sama mis i = 1 + 2
		// sama asi miinusega

		for (int i = 0; i <= 10; i += 2) {
			System.out.println(i);

		}

		// teine variant
		for (int i = 0; i <= 10; i++) {
			if (i % 2 == 0)
				System.out.println(i);

		}

	}

}
