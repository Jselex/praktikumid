package praktikum5;

public class Tabel22ristega {

	public static void main(String[] args) {

		int tabeliSuurus = 7;

		for (int j = 0; j < tabeliSuurus; j++) {
			for (int i = 0; i < tabeliSuurus; i++) {
				if (i == j || i + j == tabeliSuurus - 1) { // teine diagonaal
															// tuleb välja i ja
															// j liites, võrduma
															// tabelisuurusega
					System.out.print("x ");
				} else
					System.out.print("0 ");
			}
			System.out.println(); // reavahetus
		}

	}

}
