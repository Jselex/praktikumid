package praktikum5;

public class Tsykkel {

	public static void main(String[] args) {

		if (true) {
			System.out.println("lased kui on tõene");
		}

		int j = 10;
		while (j > 0) {
			System.out.println("laused kui on tõene (while)");
			System.out.println("i väärtus on: " + j);
			j--;
			break; // lõpetab tsükkli
		}

		for (int i = 0; i < 10; i++) {
			System.out.println("for tsükkel, i väärtus: " + i);
		}

	}

}
