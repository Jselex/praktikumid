package praktikum11;

import java.applet.Applet;
import java.awt.*;

public class Yleminek extends Applet {

	public void paint(Graphics g) {

		int h = getHeight();
		int w = getWidth();

		for (int y = 0; y < h; y++) {
			double concentrate = (double) y / h;
			int juice = (int) (concentrate * 255);
			Color color = new Color(juice, juice, 0);
			
			g.setColor(color);

			g.drawLine(0, y, w, y);

		}

	}
}