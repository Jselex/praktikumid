package praktikum11;

import java.util.Locale;
import java.util.Random;

import lib.TextIO;

public class TestingExercises {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//===================string change in main=======================
		String s = "yo-dude: like, ... []{}This Is A String";
		s = s.replaceAll("[^a-zA-Z]", "*");
		System.out.println("yo-dude: like, ... []{}this is a string ==> " + s);
		
//====================== real main=======================		
		
		System.out.println(largestPrimeFactor(53463) + "/n");
		System.out.println(greatestPrimeFactor(53463)+"");
		
		double [] tere = new double[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		System.out.println(allaKeskmise(tere));
		
		
		
	}// main
//=========================prime factor internet===========================================
	public static int largestPrimeFactor(long number) {
		int i;
		long copyOfInput = number;
		for (i = 2; i <= copyOfInput; i++) {
			if (copyOfInput % i == 0) {
				copyOfInput /= i;
				i--;
			}
		}
		return i;
	}
//=======================prime factor school================================================
	public static int greatestPrimeFactor(int n) {
		for (int i = n; i > 0; i--) {
			if (isPrime(i) && n % i == 0) {
				return i;
			}
		}

		return 1;
	}

	public static boolean isPrime(int n) {
		for (int i = 2; i < n; i++) {
			if (n % i == 0)
				return false;
		}
		return true;
	}
	
//==================alla keskmise================================	
	
	 public static int allaKeskmise (double[] d) {
			double keskmine = 0;
			int elementideHulk = 0;
			
			for (int i = 0; i < d.length; i++) {
				keskmine += d[i];
			}
			keskmine = keskmine / d.length;
			
			for (int i = 0; i < d.length; i++) {
				if (d[i] < keskmine) {
					elementideHulk++;
				}
			}
			return elementideHulk;
	   }
	
}
