package praktikum11;

public class ArrayLargest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] massiiv = new int[] { 1, 3, 6, 7, 8, 3, 53, 7, 21, 3 };
		int max = Integer.MAX_VALUE;
		for (int i = 0; i < massiiv.length; i++) {
			if (massiiv[i] <max) {
				max = massiiv[i];
			}
		}
		System.out.print(max);
		System.out.println(" ");

		int[][] neo = { { 1, 3, 6, 7 }, { 2, 3, 2, 3}, { 17, 4, 5, 34 },
				{ -20, 13, 16, 17 } };
		int maks = Integer.MIN_VALUE;
		for (int i = 0; i < neo.length; i++) {
			for (int j = 0; j < neo.length; j++) {
				if(neo[i][j] > maks){
					maks = neo[i][j];
				}
			}
		}
		System.out.print(maks);

	}

}
