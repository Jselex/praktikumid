package praktikum11;

import java.applet.Applet;
import java.awt.*;

public class PudeliHari extends Applet {
	/*
	 * Ringjoone vo~rrand parameetrilisel kujul x = r * cos(t) y = r * sin(t) t
	 * = -PI..PI
	 */
	public void paint(Graphics g) {
		int w = getWidth();
		int h = getHeight();
		int x0 = w / 2; // Keskpunkt
		int y0 = h / 2;
		int r = Math.min(x0, y0); // Raadius
		int x, y;
		double t;

		// Kysime kui suur aken on?
		

		// Ta"idame tausta
		g.setColor(Color.white);
		g.fillRect(0, 0, w, h);

		// Joonistame
		
		int n = 16;
		
		g.setColor(Color.black);
		for (t = -Math.PI; t < Math.PI; t = t + Math.PI / n) {
			x = (int) (r * Math.cos(t) + x0);
			y = (int) (r * Math.sin(t) + y0);
			g.drawLine(x0, y0, x, y);
			g.drawRect(30, 25, 45, 35);
			g.drawRect(78, 25, 45, 35);
			g.drawLine(75, 55, 78, 55);
			g.drawRect(126, 25, 45, 35);
			g.drawLine(123, 55, 125, 55);
		}
		
		g.setColor(Color.white);
		g.fillOval(100, 100, 100, 100);
		
	}
}