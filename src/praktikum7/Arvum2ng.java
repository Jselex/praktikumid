package praktikum7;

import lib.TextIO;

public class Arvum2ng {

	public static void main(String[] args) {

		int arv;

		arv = (int) (Math.random() * 101);
		
		System.out.print("Tuleb arvata arv 0'st 100'ni.\r");
		System.out.print("sisesta arv: ");
		int minu = TextIO.getlnInt();

		// seda saab ka if, else if ja else abil teha
		// see panna while tsüklisse
		while (true) {
			if (minu == arv) {
				System.out.print("Õige vastus");
              break;
			} else if (minu < arv) {
				System.out.print("Sinu arv on väiksem, proovi uuesti: ");
				minu = TextIO.getInt();
			}

			else {
				System.out.print("Sinu arv on suurem, proovi uuesti: ");
				minu = TextIO.getInt();

			}

		}

	}

}
