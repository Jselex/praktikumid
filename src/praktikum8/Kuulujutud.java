package praktikum8;

public class Kuulujutud {

	public static void main(String[] args) {

		String[] naised = { "Kelly", "Mari", "Milvi" };
		String[] mehed = { "Andres", "Andri", "Meelis" };
		String[] sonad = { "on veidrad", "käisid kinos", "on hullud" };

		int suvalinemees = (int) (Math.random() * mehed.length);
		int suvalinenaine = (int) (Math.random() * naised.length);
		int suvalinesona = (int) (Math.random() * sonad.length);

		System.out.println(naised[suvalinenaine] + " ja " + mehed[suvalinemees] + " " + sonad[suvalinesona]);

	}

}
