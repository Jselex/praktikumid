package praktikum8;


public class Massiiv1 {

	public static void main(String[] args) {

		// int[] arvud = new int[10]; //Massiiv ja mälu eraldamine, ehk kui
		// palju elemente massiivi tuleb
		// indexid on 0.. massiivi pikkus -1, nt 10 indexid on 0-9.
		// arvud[0] = 123;
		// arvud[1] = 12;
		// arvud[2] = 13;
		
		int[] arvud = {11, 22, 33};
        System.out.println(arvud[1]);
        
        for (int i=0; i <= arvud.length; i++){
        	 System.out.println(arvud[i]);
        	
        }
 	}

}
