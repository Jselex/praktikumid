package praktikum8;

import lib.TextIO;

public class KymmeArvu {

	public static void main(String[] args) {

		int[] arvud = new int[10];

		for (int i = 0; i < arvud.length; i++) {
			System.out.println("Sisesta sõna " + i);
			arvud[i] = TextIO.getlnInt();
		}

		for (int i = 9; i >= 0; i--) {
			System.out.println(arvud[i]);
		}

	}

}
