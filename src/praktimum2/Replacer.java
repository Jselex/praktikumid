package praktimum2;

import lib.TextIO;

public class Replacer {

	public static void main(String[] args) {

		String nimi;

		System.out.print("Please enter your name: ");
		nimi = TextIO.getlnString();

		nimi = nimi.replace('a', '_');

		System.out.print(nimi);

	}

}
