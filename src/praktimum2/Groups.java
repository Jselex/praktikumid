package praktimum2;

import lib.TextIO;

public class Groups {

	public static void main(String[] args) {
	
		int people;
		int groups;
		int remainder;
		
		System.out.print("Enter the number of people ");
		people = TextIO.getlnInt();
		
		System.out.print("Enter the number of groups to make ");
	    groups = TextIO.getlnInt();
	    
		
	    remainder = people % groups;
		groups = people / groups;
	
		
		System.out.print("The number of groups created is ");
		System.out.print(groups);
		
		System.out.print("The remainder is ");
		System.out.print(remainder);
	}

}
