package praktimum2;

import lib.TextIO;

public class Namelenght {

	public static void main(String[] args) {

		String nimi;

		System.out.print("Please enter your name: ");
		nimi = TextIO.getlnString();

		int nimePikkus = nimi.length();
		System.out.print("The name " + nimi + " has " + nimePikkus + " letters.");

	}

}
