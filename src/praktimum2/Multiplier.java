package praktimum2;

import lib.TextIO;

public class Multiplier {

    public static void main(String[] args) {

        double tegur1;  
        double tegur2;
        double answer;

        System.out.print("Enter first multiplier ");
        tegur1 = TextIO.getlnDouble(); 
        
        System.out.print("Enter second multiplier ");
        tegur2 = TextIO.getlnDouble();

        answer = tegur1 * tegur2;
        System.out.print("The answer is " + answer);
  

    }

}