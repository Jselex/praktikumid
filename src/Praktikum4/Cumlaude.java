package Praktikum4;

import lib.TextIO;

public class Cumlaude {

	public static void main(String[] args) {
		
		double keskmine;
		int loputoo;
	
		
		
		System.out.println("Sisesta keskmine hinne: ");
		keskmine = TextIO.getlnDouble();
		System.out.println("Sisesta lõputöö hinne: ");
		loputoo = TextIO.getlnInt();
		
		if (loputoo < 0 || loputoo > 5) {// sisestada alguses valede sisestuste välistus
			 System.out.println("vigane sisestus");
			 return; // väljub meetodist
			}	
			if (keskmine < 0 || keskmine > 5){
				System.out.println("vigane sisestus");
			 return;
			}
		
        if(keskmine >= 4.5 && loputoo == 5){
        	System.out.println("Saad Cum Laude!");
        } 
        else{
             	System.out.println("Ei saa Cum Laude!");
        }		
       
	}
	

}
