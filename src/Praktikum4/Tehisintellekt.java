package Praktikum4;

import lib.TextIO;

public class Tehisintellekt {

	public static void main(String[] args) {
		
		int age1;
		int age2;
		
		
		System.out.println("sisesta esimene vanus: ");
		age1 = TextIO.getlnInt();
		System.out.println("sisesta teine vanus: ");
		age2 = TextIO.getlnInt();
		
		int vanusevahe = Math.abs(age1 - age2);
		
		if(vanusevahe > 10){
			System.out.println("Midagi väga krõbedat");
		}
		else if(vanusevahe > 5){
			System.out.println("ikka ei sobi");
		}
		else{
			System.out.println("sobib!");
		}
		
	}

}
