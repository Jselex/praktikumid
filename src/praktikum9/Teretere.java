package praktikum9;

import lib.TextIO;

public class Teretere {

	public static void main(String[] args) {
		
		System.out.println("Sisesta üks sõna");
		String sona = TextIO.getlnString();
		sona = sona.toUpperCase();
		System.out.println(sona);
		
		int i;
		for(i = 0; i < sona.length(); i = i + 1) {
			System.out.print(sona.charAt(i));
			if (i < sona.length() - 1){
				System.out.print("-");
			}
		}
	} 

}
